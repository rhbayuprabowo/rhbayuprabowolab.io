# rhbayuprabowo.gitlab.io

A programmer who have a passion for building a desktop &amp; web app. Right now, I'm excited to learn more about web development technologies.

## Motivation

This project was created to promote myself as junior developer and showcase of my projects.

## Built With

* [Materialize](http://materializecss.com/) - A modern responsive front-end framework based on Material Design.
* [EmailJS](https://www.emailjs.com/) - Send email directly from Javascript. No server code needed. Focus on things that matter!
* [TypeItJS](https://typeitjs.com/) - TypeIt is the most versatile JavaScript utility on the planet for creating dynamic, realistic typewriter text effects.
* [JQuery](https://jquery.com/) - jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. 

## Authors

* **R. H. Bayu Prabowo** - [rhbayuprabowo](https://github.com/rhbayuprabowo)

## License

This project is licensed under the MIT License
