const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const sass        = require('gulp-sass');

// Compile Sass & Inject Into Browser
gulp.task('sass', function() {
    return gulp.src(['node_modules/materialize-css/sass/materialize.scss', 'scss/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("css"))
        .pipe(browserSync.stream());
});

// Move JS Files to src/js
gulp.task('js', function() {
  return gulp.src(['node_modules/materialize-css/dist/js/materialize.min.js',
                  'node_modules/jquery/dist/jquery.min.js',
                  'node_modules/typeit/dist/typeit.min.js'])
        .pipe(gulp.dest("js"))
        .pipe(browserSync.stream());
});

// Watch Sass & Serve
gulp.task('serve', ['sass'], function() {
    
  browserSync.init({
      browser: ["chrome","firefox"],
      server: "./"  
    });
    
    gulp.watch(['node_modules/materialize-css/sass/materialize.scss', 'scss/*.scss'], ['sass']);
    gulp.watch(["js/*.js", "*.html"]).on('change', browserSync.reload);
});

// Move Fonts to fonts
gulp.task('fonts', function() {
  return gulp.src('node_modules/materialize-css/dist/fonts/**/*')
    .pipe(gulp.dest('fonts'));
});


gulp.task('default', ['js','serve', 'fonts']);